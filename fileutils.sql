-- fileutils related tables

CREATE TABLE `fileutils_download_cntr` (
  `date` varchar(15) NOT NULL default '',
  `fname` varchar(255) NOT NULL default '',
  `hits` int(10) unsigned NOT NULL default '0',
  PRIMARY KEY  (`fname`,`date`)
) TYPE=MyISAM;

CREATE TABLE `fileutils_md5_cache` (
  `fname` varchar(255) NOT NULL default '',
  `timestamp` int(11) NOT NULL default '0',
  `md5` varchar(255) NOT NULL default '',
  PRIMARY KEY  (`fname`)
) TYPE=MyISAM;
